function Enemy(x, y, c, range){
  this.x = x;
  this.y = y;
  this.c = c;
  this.range = range;
  this.angle = 0;
  this.vx = 0.06;
  this.width = 40;
  this. height = 33;
  console.log("enemie created, x = "+this.x+", y = "+this.y+", c = "+this.c+", range = "+this.range);
}

function Enemy(x, y,velocity,range,proportion,inclination){
  this.width = 40;
  this.height = 33;
  this.x = x - this.width/2;
  this.y = y - this.height/2;
  this.velocity = velocity;
  this.range = range;
  this.proportion = proportion;
  this.inclination = (inclination+1)*(Math.PI);
  this.angle = 0;
  this.centerX = this.x - range*Math.cos(this.inclination);
  this.centerY = this.y + range*Math.sin(this.inclination);
  this.velocity = velocity;

  //console.log("enemie created, x = "+this.x+", y = "+this.y+", c = "+this.c+", range = "+this.range);
}


Enemy.prototype.move= function() {
   
    //this.x = (this.c - this.width/2) + Math.sin(this.angle) * (this.range - this.width/2);     
    //this.angle += (this.vx * Math.random());
	
	this.angle += this.velocity;
	var i = this.range*Math.cos(this.angle);
	var j = this.proportion*this.range*Math.sin(this.angle);
	this.x = i*Math.cos(this.inclination) + j*Math.sin(this.inclination) + this.centerX;
	this.y = -i*Math.sin(this.inclination) + j*Math.cos(this.inclination) + this.centerY;
};

Enemy.prototype.draw= function(context){
    context.save();
    //context.translate(this.x, this.y);
    //context.rotate(this.rotation);
    //context.scale(this.scaleX, this.scaleY);
    context.lineWidth = 3;
    context.beginPath();
    //context.fillStyle = 'black';
	//context.fillRect(this.x,this.y - .5*this.height,this.width,this.height);
	var img = new Image();
    img.src = "Images/enemy.png";
	context.drawImage(img, this.x, this.y - .5*this.height,this.width, this.height);
    context.closePath();
    context.restore();
    };
    

   
