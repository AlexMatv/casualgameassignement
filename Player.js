var stopped = 0,
    running = 1,
    jumping = 2,
    falling = 3;
    sliding = 4;

function player(){
    this.x;
    this.y;
    this.Width;
    this.Height;
    this.width;
    this.height;
    this.speed;
    this.velX;
    this.velY;
    this.grounded;
    this.behavior;
    this.friction;
    this.gravity;
    this.holdingKey;
	this.img;
	this.health = 10;
	this.right;
}

player.prototype.start = function(initialX,initialY){
	this.x = initialX;
    this.y = initialY;
    this.Width= 30;
    this.Height= 45;
    this.width= this.Width;
    this.height= this.Height;
    this.speed= 5;
    this.velX= 0;
    this.velY= 0;
    this.grounded= false;
    this.behavior = stopped;
    this.friction = .8;
    this.gravity = 0.3;
    this.holdingKey = null;
	this.right = true;
	this.img = new Image();
	this.iobj = {
			  source: null,
			  current: 0,
			  total_frames: 8,
			  width: 100,
			  height: 150,
			  interval: 0
			};
}



function dead() {
	mySounddie.play();//sound after death of the character
// we need to add some funny picture for dead of the player 
}

player.prototype.updateState = function(keys){
	
    upArrow = keys[38];
    rightArrow = keys[39];
    leftArrow = keys[37];
    downArrow = keys[40];
    space = keys[32];
	myMusic.play();//background music
    
    if (upArrow && !downArrow) {
        if (this.grounded) {
			mySoundjump.play();
            //this.behavior = jumping;
            this.grounded = false;
            this.velY = -this.speed * 2;
        }
    }

    if (rightArrow) {
        //move to Right
        if (this.velX < this.speed) {
            this.velX++;
			this.right = true;
        }
        //
    }
    if (leftArrow) {
        //move to Left
        if (this.velX > -this.speed) {
            this.velX--;
			this.right = false;
        }
        //
    }

    if (downArrow && (leftArrow || rightArrow)) {
        if (this.grounded) {
            this.behavior = sliding;
            this.color = "black";
        }
    }
	
	if(space){
		this.dropCoin();
	}
    
    var minVelocity = .3;
    if((Math.abs(this.velX) < minVelocity) && this.grounded){
        this.behavior = this.changeBehavior(stopped);
        this.color = "white";
    }
	
    if(!this.grounded && (this.velY < 0)){
        this.behavior = this.changeBehavior(jumping);
        this.color = "blue";
    }
    
    if(!this.grounded && (this.velY > 0)){
        this.behavior = this.changeBehavior(falling);
        this.color = "green";
    }
    
    if(this.grounded && Math.abs(this.velX) > minVelocity && !downArrow){
        this.behavior = this.changeBehavior(running);
        this.color = "red";
    }
    
    if(this.grounded){
        this.velY = 0;
    }
    
    this.velX *= this.friction;
    this.velY += this.gravity;
    this.x += this.velX;
    this.y += this.velY;
    
    
//    
//    if(!this.grounded && this.velY>=0){
//        this.jumping = false;
//        this.falling = true;
//    }
//    
//    if(this.sliding){
//        this.width= this.Height;
//        this.height= this.Width; 
//    }
//    else{
//        this.width= this.Width;
//        this.height= this.Height;
//    }
};
player.prototype.changeBehavior = function(behavior){
	if(behavior != this.behavior){
			this.iobj.current = 0;
			this.iobj.interval = 0;
	}
	return(behavior);
}
player.prototype.draw= function(context){
    context.save();
    context.translate(this.x, this.y);
    context.rotate(this.rotation);
    context.scale(this.scaleX, this.scaleY);
    context.fillStyle = this.color;
    context.beginPath();

	var img = new Image();
		
		switch(this.behavior){
			
			case(stopped):
				img.src = 'Images/player/movements_Player_stopped_mirrored.png';
				this.iobj.total_frames = 1;
				this.iobj.width = 100;
				this.iobj.height = 150;
			break;
			case(running):
				img.src = 'Images/player/walk_Player_walk_mirrored.png';
				this.iobj.total_frames = 8;
				this.iobj.width = 100;
				this.iobj.height = 150;
			break;
			case(jumping):
				img.src = 'Images/player/movements_Player_jumping_mirrored.png';
				this.iobj.total_frames = 2;
				this.iobj.width = 140;
				this.iobj.height = 150;
				this.iobj.interval = 0;
				if(Math.abs(this.velY) < 3){
					this.iobj.current = 1;
				}
				else{
					this.iobj.current = 0;
				}
			break;
			case(falling):
				img.src = 'Images/player/movements_Player_falling_mirrored.png';
				this.iobj.total_frames = 3;
				this.iobj.width = 140;
				this.iobj.height = 150;
				this.iobj.interval = 0;
				if(Math.abs(this.velY) < 3){
					this.iobj.current = 0;
				}
				else{
					if(Math.abs(this.velY) < 5 ){
						this.iobj.current = 1;
					}
					else{
						this.iobj.current = 2;	
					}
				}
			break;
		}

		this.iobj.source = img; // we set the image source for our object.
		
	  if (this.iobj.source != null)
		  
		if(this.right){
			var X = this.iobj.width*this.iobj.current;
			var Y = 0;
		}
		else{
			var X = this.iobj.width*(this.iobj.total_frames - this.iobj.current -1);
			var Y = this.iobj.height;					
		}
		
		context.drawImage(this.iobj.source, X, Y, this.iobj.width, this.iobj.height, 0, 0, this.width, this.height);
		this.iobj.interval += 1;
		if(this.iobj.interval > 3){
			this.iobj.current = ((this.iobj.current + 1) % this.iobj.total_frames);
			this.iobj.interval = 0;
		}
		
	  
	  // incrementing the current frame and assuring animation loop
			

	//---------------------------------------
	//var img = new Image();
	//img.src = "Images/char.png";
	//context.drawImage(img_obj, 0, 0);
	context.closePath();
    context.restore();
};

player.prototype.dropCoin= function(){
	if(this.holdingKey){
		var coin = this.holdingKey;
		coin.visible = true;
	}
	this.holdingKey = null;
}