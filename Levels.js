var background1 = "Images/background1.jpg",
	background2 = "Images/background2.jpg",
	background3 = "Images/background3.jpg",
	background4 = "Images/background4.jpg";

function loadLevel(levelNumber){
	switch (levelNumber){
	case 'menu':
		level = new levelMenu();
	break;	
	case '1-1':
		level = new level1o1();
	break;
	case '1-2':
		level = new level1o2();
	break;
	case '1-3':
		level = new level1o3();
	break;
	case '2-1':
		level = new level2o1();
	break;
	case '2-2':
		level = new level2o2();
	break;
	case '2-3':
		level = new level2o3();
	break;
	case '3-1':
		level = new level3o1();
	break;
	case '3-2':
		level = new level3o2();
	break;
	case '3-3':
		level = new level3o3();
	break;
	case '4-1':
		level = new level4o1();
	break;
	case '4-2':
		level = new level4o2();
	break;
	case '4-3':
		level = new level4o3();
	break;
	}
	return level;
	console.log("level "+levelNumber+" uploaded");
}

function levelMenu(){
	this.eqDifficulty = "easy";
	this.background = 'Images/bg.bmp';
    this.boxes = [];//platforms and borders
	this.coinPlaces = [];
	this.enemies = [];
	this.doors = [];
	
	this.initialX = 0;
	this.initialY = 500;
	
    this.boxes.push({
        x: 100,
        y: 180,
        width: 250,
        height: 20
    });  
    this.boxes.push({
        x: 470,
        y: 250,
        width: 250,
        height: 20
    });
    this.boxes.push({
        x: 100,
        y: 370,
        width: 250,
        height: 20
    });
    this.boxes.push({
        x: 470,
        y: 450,
        width: 250,
        height: 20
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    

	this.doors.push({
        x: 650,
        y: 350,
		nextLevel: '1-1',
		Text: "Universe 1"
    });
	this.doors.push({
        x: 100,
        y: 250,
		nextLevel: '2-1',
		Text: "Universe 2"
    });
	this.doors.push({
        x: 650,
        y: 100,
		nextLevel: '3-1',
		Text: "Universe 3"
    });
	this.doors.push({
        x: 100,
        y: 50,
		nextLevel: '4-1',
		Text: "Universe 4"
    });
	
}


//An industrial version level 1-1
function level1o1(){
	console.log("level 1 called");
	this.limitTime = 30;//seconds
	this.background = background1;
	this.eqDifficulty = "easy";
	this.nextLevel = '1-2';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 150,
        y: 150,
        width: 200,
        height: 20
    });
    this.boxes.push({
        x: 500,
        y: 250,
        width: 250,
        height: 20
    });
    this.boxes.push({
        x: 60,
        y: 370,
        width: 300,
        height: 20
    });
    this.boxes.push({
        x: 470,
        y: 450,
        width: 100,
        height: 20
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: this.boxes[0].x,
        y: this.boxes[0].y,
		velocity: 0.03,
		range: (this.boxes[0].width)/2,
		proportion: 0,
		inclination: 0
        //c: this.boxes[0].x + (this.boxes[0].width)/2,
        //range: (this.boxes[0].width)/2 
    });
	this.enemies.push({
        x: this.boxes[3].x+25,
        y: this.boxes[3].y,
		velocity: 0.03,
		range: 70,
		proportion: 0,
		inclination: 0.5
    });

    this.doors = [];

	this.doors.push({
        x: 700,
        y: 50,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 240,
        y: 50
    });
    this.coinPlaces.push({
        x: 600,
        y: 100
    });
    this.coinPlaces.push({
        x: 600,
        y: 330
    });
    this.coinPlaces.push({
        x: 200,
        y: 250
    });
}


//An industrial version level1-2
function level1o2(){
	console.log("level 2 called");
	this.limitTime = 30;//seconds
	this.background = background1;
	this.eqDifficulty = "medium";
	this.nextLevel = '1-3';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 120,
        y: 200,
        width: 100,
        height: 20
    });
    this.boxes.push({
        x: 500,
        y: 300,
        width: 30,
        height: 30
    });
    this.boxes.push({
        x: 100,
        y: 450,
        width: 200,
        height: 20
    });
    this.boxes.push({
        x: 600,
        y: 440,
        width: 100,
        height: 160
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height - 20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 600,
        y: 200,
        width: 30,
        height: 30
    });
    this.boxes.push({
        x: 400,
        y: 300,
        width: 30,
        height: 30
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: this.boxes[0].x+(this.boxes[0].width)/2,
        y: this.boxes[0].y+100,
		velocity: 0.03,
		range: 100,
		proportion: 1,
		inclination: 0.5
        //c: this.boxes[0].x + (this.boxes[0].width)/2,
        //range: (this.boxes[0].width)/2 
    });
    //the full version will be use in Universe 3
    /*this.enemies.push({
        x: this.boxes[1].x-100,
        y: this.boxes[1].y+17,
		velocity: 0.03,
		range: 100,
		proportion: 0,
		inclination: 0.17
    });*/
	this.enemies.push({
        x: this.boxes[2].x,
        y: this.boxes[2].y,
		velocity: 0.03,
		range: this.boxes[2].width/2,
		proportion: 0,
		inclination: 0 
    });
	this.enemies.push({
        x: this.boxes[3].x+(this.boxes[3].width)/2,
        y: this.boxes[3].y,
		velocity: 0.03,
		range: (this.boxes[3].width),
		proportion: 0,
		inclination: 0.5
    });

    this.doors = [];

	this.doors.push({
        x: 700,
        y: 50,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 160,
        y: 100
    });
    this.coinPlaces.push({
        x: 505,
        y: 130
    });
    this.coinPlaces.push({
        x: 750,
        y: 530
    });
    this.coinPlaces.push({
        x: 40,
        y: 300
    });
}


//An industrial version level 1-3
function level1o3(){
	this.limitTime = 30;//seconds
	console.log("level 3 called");
	this.background = background1;
	this.eqDifficulty = "hard";
	this.nextLevel = 'menu';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 50,
        y: 250,
        width: 100,
        height: 20
    });
    this.boxes.push({
        x: 200,
        y: 250,
        width: 100,
        height: 20
    });
    this.boxes.push({
        x: 500,
        y: 180,
        width: 200,
        height: 20
    });
    this.boxes.push({
        x: 100,
        y: 370,
        width: 150,
        height: 20
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height - 20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 380,
        y: 300,
        width: 100,
        height: 20
    });
    this.boxes.push({
        x: 580,
        y: 450,
        width: 70,
        height: 20
    });
    
    this.enemies = [];
    
    //this full version we can use in Universe 3
	/*this.enemies.push({
        x: this.boxes[1].x-175,
        y: this.boxes[1].y+50,
		velocity: 0.03,
		range: 150,
		proportion: 1,
		inclination: 0 
    });*/
	this.enemies.push({
        x: this.boxes[2].x,
        y: this.boxes[2].y,
		velocity: 0.03,
		range: this.boxes[2].width/2,
		proportion: 0,
		inclination: 0 
    });
	this.enemies.push({
        x: this.boxes[3].x+(this.boxes[3].width)/2,
        y: this.boxes[3].y,
		velocity: 0.03,
		range: (this.boxes[3].width)*1.1,
		proportion: 0,
		inclination: 0.5
    });
    this.enemies.push({
        x: this.boxes[7].x+this.boxes[7].width/2,
        y: this.boxes[7].y,
		velocity: 0.04,
		range: this.boxes[7].width*1.3,
		proportion: 0,
		inclination: 0.5
    });
	this.enemies.push({
        x: this.boxes[8].x+this.boxes[8].width/2,
        y: this.boxes[8].y,
		velocity: 0.05,
		range: this.boxes[8].width*1.5,
		proportion: 0,
		inclination: 0.5
    });
    this.doors = [];

	this.doors.push({
        x: 700,
        y: 50,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 115,
        y: 315
    });
    this.coinPlaces.push({
        x: 215,
        y: 315
    });
    this.coinPlaces.push({
        x: 115,
        y: 150
    });
    this.coinPlaces.push({
        x: 215,
        y: 150
    });
    //-------------------------------
}    

function level2o1(){
	this.limitTime = 30;//seconds
	console.log("level 1 called");
	this.background = background2;
	this.eqDifficulty = "easy";
	this.nextLevel = '2-2';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders

	this.boxes.push({
        x: 60,
        y: 470,
        width: 70,
        height: 20
    });
	
	this.boxes.push({
        x: 260,
        y: 350,
        width: 70,
        height: 20
    });
	
	this.boxes.push({
        x: 60,
        y: 230,
        width: 70,
        height: 20
    });
	
	this.boxes.push({
        x: 460,
        y: 230,
        width: 70,
        height: 20
    });
	
	this.boxes.push({
        x: 660,
        y: 110,
        width: 120,
        height: 20
    });
	
	// this.boxes.push({
        // x: 260,
        // y: 110,
        // width: 70,
        // height: 20
    // });
	
	this.boxes.push({
        x: 460,
        y: 470,
        width: 70,
        height: 20
    });
	
	this.boxes.push({
        x: 660,
        y: 350,
        width: 70,
        height: 20
    });

    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height - 20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: this.boxes[0].x,
        y: this.boxes[0].y,
		velocity: 0.057,
		range: (this.boxes[0].width)/2,
		proportion: 0,
		inclination: 0
        //c: this.boxes[0].x + (this.boxes[0].width)/2,
        //range: (this.boxes[0].width)/2 
    });
	
	
	this.enemies.push({
        x: this.boxes[3].x ,
        y: this.boxes[3].y,
		velocity: 0.0556,
		range: (this.boxes[0].width)/2,
		proportion: 0,
		inclination: 0
	});
	
	this.enemies.push({
        x: this.boxes[5].x,
        y: this.boxes[5].y,
		velocity: 0.0559,
		range: (this.boxes[0].width)/2,
		proportion: 0,
		inclination: 0
	});
 
    this.doors = [];

	this.doors.push({
        x: 720,
        y: 30,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 78,
        y: 155
    });
    this.coinPlaces.push({
        x: 675,
        y: 280
    });
    this.coinPlaces.push({
        x: 675,
        y: 400
    });
	this.coinPlaces.push({
        x: 78,
        y: 275
    });
}

function level2o2(){
this.limitTime = 30;//seconds
	console.log("level 2 called");
	this.background = background2;
	this.eqDifficulty = "medium";
	this.nextLevel = '2-3';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 100,
        y: 180,
        width: 170,
        height: 20
    });
    this.boxes.push({
        x: 530,
        y: 180,
        width: 170,
        height: 20
    });
    this.boxes.push({
        x: 100,
        y: 370,
        width: 170,
        height: 20
    });
    this.boxes.push({
        x: 530,
        y: 370,
        width: 170,
        height: 20
    });
	this.boxes.push({
        x: 375,
        y: 470,
        width: 50,
        height: 20
    });
    this.boxes.push({
        x: 375,
        y: 270,
        width: 50,
        height: 20
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: this.boxes[0].x,
        y: this.boxes[0].y,
		velocity: 0.04,
		range: (this.boxes[0].width)/2,
		proportion: 0,
		inclination: 0
    });
	
    this.enemies.push({
        x: 250,
        y: 350,
		velocity: 0.032,
		range: 200,
		proportion: 0,
		inclination: 0.25
    });
	
	 this.enemies.push({
        x: 250,
        y: 550,
		velocity: 0.03,
		range: 200,
		proportion: 0,
		inclination: 0.25
        //c: this.boxes[0].x + (this.boxes[0].width)/2,
        //range: (this.boxes[0].width)/2 
    });

    this.doors = [];

	this.doors.push({
        x: 100,
        y: 50,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 640,
        y: 90
    });
    this.coinPlaces.push({
        x: 640,
        y: 280
    });
    this.coinPlaces.push({
        x: 120,
        y: 280
    });
	this.coinPlaces.push({
        x: 380,
        y: 300
    });
}

function level2o3(){
	this.limitTime = 30;//seconds
	console.log("level 3 called");
	this.background = background2;
	this.eqDifficulty = "hard";
	this.nextLevel = 'menu';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 275,
        y: 450,
        width: 250,
        height: 20
    });
    this.boxes.push({
        x: 100,
        y: 300,
        width: 120,
        height: 20
    });
    this.boxes.push({
        x: 580,
        y: 300,
        width: 120,
        height: 20
    });
    this.boxes.push({
        x: 275,
        y: 150,
        width: 250,
        height: 20
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: 240,
        y: 460,
		velocity: 0.08,
		range: 70,
		proportion: 0,
		inclination: -0.5 
    });
	
	this.enemies.push({
        x: 560,
        y: 440,
		velocity: 0.08,
		range: 70,
		proportion: 0,
		inclination: -0.5
    });
    
	this.enemies.push({
        x: 300,
        y: 325,
		velocity: 0.035,
		range: 100,
		proportion: 1,
		inclination: 0
    });
	
	this.enemies.push({
        x: 90,
        y: 275,
		velocity: 0.05,
		range: 80,
		proportion: 0,
		inclination: 0
    });
	
	this.enemies.push({
        x: 560,
        y: 275,
		velocity: 0.05,
		range: 80,
		proportion: 0,
		inclination: 0
    });

    this.doors = [];

	this.doors.push({
        x: 365,
        y: 50,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 300,
        y: 480
    });
    // this.coinPlaces.push({
        // x: 380,
        // y: 480
    // });
    this.coinPlaces.push({
        x: 460,
        y: 480
    });
    this.coinPlaces.push({
        x: 380,
        y: 280
    });
}

function level3o1(){
	this.limitTime = 30;//seconds
	console.log("level 1 called");
	this.background = background3;
	this.eqDifficulty = "easy";
	this.nextLevel = '3-2';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders

	this.boxes.push({
        x: 144,
        y: 460,
        width: 20,
        height: 140
    });
	
	this.boxes.push({
        x: 308,
        y: 460,
        width: 20,
        height: 140
    });
	
	this.boxes.push({
        x: 472,
        y: 460,
        width: 20,
        height: 140
    });
	
	this.boxes.push({
        x: 636,
        y: 460,
        width: 20,
        height: 140
    });
	
	this.boxes.push({
        x: 380,
        y: 310,
        width: 40,
        height: 20
    });
	
	this.boxes.push({
        x: 144,
        y: 210,
        width: 40,
        height: 20
    });
	
	this.boxes.push({
        x: 616,
        y: 210,
        width: 40,
        height: 20
    });

    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: 144,
        y: 270,
		velocity: 0.057,
		range: 130,
		proportion: 0,
		inclination: -0.25 
    });
	
	this.enemies.push({
        x: 665,
        y: 270,
		velocity: 0.057,
		range: 130,
		proportion: 0,
		inclination: -0.75 
    });
 
    this.doors = [];

	this.doors.push({
        x: 365,
        y: 30,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 215,
        y: 450
    });
    this.coinPlaces.push({
        x: 379,
        y: 450
    });
    this.coinPlaces.push({
        x: 543,
        y: 450
    });
	// this.coinPlaces.push({
        // x: 78,
        // y: 275
    // });
}

function level3o2(){
	this.limitTime = 30;//seconds
	console.log("level 2 called");
	this.background = background3;
	this.eqDifficulty = "medium";
	this.nextLevel = '3-3';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 300,
        y: 450,
        width: 200,
        height: 20
    });
    this.boxes.push({
        x: 350,
        y: 300,
        width: 100,
        height: 20
    });
    this.boxes.push({
        x: 230,
        y: 180,
        width: 70,
        height: 20
    });
	this.boxes.push({
        x: 490,
        y: 180,
        width: 70,
        height: 20
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: this.boxes[0].x,
        y: this.boxes[0].y,
		velocity: 0.052,
		range: (this.boxes[0].width)/2,
		proportion: 0,
		inclination: 0
    });
	this.enemies.push({
        x: 70,
        y: 570,
		velocity: 0.05,
		range: 100,
		proportion: 0,
		inclination: 0
    });
	this.enemies.push({
        x: 550,
        y: 570,
		velocity: 0.05,
		range: 100,
		proportion: 0,
		inclination: 0
    });
	this.enemies.push({
        x: 300,
        y: 300,
		velocity: 0.05,
		range: 100,
		proportion: 1,
		inclination: 0
    });
	
    this.enemies.push({
        x: 70,
        y: 260,
		velocity: 0.054,
		range: 100,
		proportion: 0,
		inclination: 0
    });
	this.enemies.push({
        x: 550,
        y: 260,
		velocity: 0.054,
		range: 100,
		proportion: 0,
		inclination: 0
    });

    this.doors = [];

	this.doors.push({
        x: 360,
        y: 490,
		nextLevel: this.nextLevel
    });
	
    this.coinPlaces = [];
    
    // this.coinPlaces.push({
        // x: 640,
        // y: 90
    // });
    this.coinPlaces.push({
        x: 640,
        y: 50
    });
    this.coinPlaces.push({
        x: 120,
        y: 50
    });
	this.coinPlaces.push({
        x: 380,
        y: 50
    });
}

function level3o3(){
	this.limitTime = 30;//seconds
	console.log("level 3 called");
	this.background = background3;
	this.eqDifficulty = "hard";
	this.nextLevel = 'menu';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 5,
        y: 150,
        width: 750,
        height: 20
    });
    this.boxes.push({
        x: 45,
        y: 300,
        width: 750,
        height: 20
    });
    this.boxes.push({
        x: 5,
        y: 450,
        width: 750,
        height: 20
    });

    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: 270,
        y: 510,
		velocity: 0.08,
		range: 40,
		proportion: 0,
		inclination: -0.65
    });
	
	this.enemies.push({
        x: 520,
        y: 510,
		velocity: 0.085,
		range: 40,
		proportion: 0,
		inclination: -0.65
    });
    
	    this.enemies.push({
        x: 170,
        y: 360,
		velocity: 0.075,
		range: 40,
		proportion: 1,
		inclination: -0.35
    });
	
	this.enemies.push({
        x: 370,
        y: 360,
		velocity: 0.08,
		range: 40,
		proportion: 1,
		inclination: -0.35
    });
    
	this.enemies.push({
        x: 570,
        y: 360,
		velocity: 0.085,
		range: 40,
		proportion: 1,
		inclination: -0.35
    });
	
	this.enemies.push({
        x: 150,
        y: 300,
		velocity: 0.07,
		range: 45,
		proportion: 0,
		inclination: 0.5
    });

	this.enemies.push({
        x: 300,
        y: 300,
		velocity: 0.075,
		range: 45,
		proportion: 0,
		inclination: 0.5
    });
	
	this.enemies.push({
        x: 450,
        y: 300,
		velocity: 0.08,
		range: 45,
		proportion: 0,
		inclination: 0.5
    });
    
	this.enemies.push({
        x: 600,
        y: 300,
		velocity: 0.085,
		range: 45,
		proportion: 0,
		inclination: 0.5
    });

    this.doors = [];

	this.doors.push({
        x: 15,
        y: 65,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 350,
        y: 20
    });
    this.coinPlaces.push({
        x: 440,
        y: 20
    });
    this.coinPlaces.push({
        x: 530,
        y: 20
    });
}    

function level4o1(){
		this.limitTime = 30;//seconds
	console.log("level 1 called");
	this.background = background4;
	this.eqDifficulty = "easy";
	this.nextLevel = '4-2';
	
	this.initialX = 0;
	this.initialY = 500;

    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 200,
        y: 200,
        width: 50,
        height: 50
    });
    this.boxes.push({
        x: 500,
        y: 200,
        width: 50,
        height: 50
    });
    this.boxes.push({
        x: 200,
        y: 430,
        width: 50,
        height: 50
    });
    this.boxes.push({
        x: 500,
        y: 430,
        width: 50,
        height: 50
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 350,
        y: 315,
        width: 50,
        height: 50
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: this.boxes[0].x-25,
        y: this.boxes[0].y+25,
		velocity: 0.03,
		range: (this.boxes[0].width),
		proportion: 2.5,
		inclination: 0
        //c: this.boxes[0].x + (this.boxes[0].width)/2,
        //range: (this.boxes[0].width)/2 
    });
    this.enemies.push({
        x: this.boxes[1].x-25,
        y: this.boxes[1].y+25,
		velocity: 0.03,
		range: (this.boxes[1].width),
		proportion: 2.5,
		inclination: 0
    });
	this.enemies.push({
        x: this.boxes[2].x-25,
        y: this.boxes[2].y+25,
		velocity: 0.03,
		range: (this.boxes[2].width),
		proportion: 2.5,
		inclination: 0
    });
	this.enemies.push({
        x: this.boxes[3].x-25,
        y: this.boxes[3].y+25,
		velocity: 0.03,
		range: (this.boxes[3].width),
		proportion: 2.5,
		inclination: 0
    });

    this.doors = [];

	this.doors.push({
        x: 700,
        y: 50,
		nextLevel: this.nextLevel
    });
	
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 205,
        y: 120
    });
    this.coinPlaces.push({
        x: 505,
        y: 120
    });
    this.coinPlaces.push({
        x: 205,
        y: 370
    });
    this.coinPlaces.push({
        x: 505,
        y: 370
    });
}

function level4o2(){
	this.limitTime = 30;//seconds
	console.log("level 2 called");
	this.background = background4;
	this.eqDifficulty = "medium";
	this.nextLevel = '4-3';
	this.initialX = 0;
	this.initialY = 500;
	
    this.boxes = [];
    this.boxes.push({
        x: 250,
        y: 450,
        width: 20,
        height: 150
    });
    this.boxes.push({
        x: 400,
        y: 350,
        width: 20,
        height: 150
    });
    this.boxes.push({
        x: 550,
        y: 250,
        width: 20,
        height: 150
    });
    this.boxes.push({
        x: 330,
        y: 170,
        width: 40,
        height: 40
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 720,
        y: 250,
        width: 20,
        height: 150
    });
    
    this.enemies = [];
    
    this.enemies.push({
        x: this.boxes[0].x+100,
        y: this.boxes[0].y+60,
		velocity: 0.08,
		range: (this.boxes[0].width)*5.7,
		proportion: 0,
		inclination: 0.5
        //c: this.boxes[0].x + (this.boxes[0].width)/2,
        //range: (this.boxes[0].width)/2 
    });
    this.enemies.push({
        x: this.boxes[1].x+100,
        y: this.boxes[1].y+60,
		velocity: 0.07,
		range: (this.boxes[1].width)*6,
		proportion: 0,
		inclination: 0.5
    });
	this.enemies.push({
        x: this.boxes[2].x+100,
        y: this.boxes[2].y+60,
		velocity: 0.06,
		range: (this.boxes[2].width)*6,
		proportion: 0,
		inclination: 0.5
	});
	this.enemies.push({
        x: this.boxes[3].x-60,
        y: this.boxes[3].y+20,
		velocity: 0.03,
		range: (this.boxes[3].width)*2,
		proportion: 0.75,
		inclination: 0
    });
    

	this.doors = [];

	this.doors.push({
        x: 100,
        y: 50,
		nextLevel: this.nextLevel
    });
    
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 330,
        y: 50
    });
    this.coinPlaces.push({
        x: 750,
        y: 350
    });
    this.coinPlaces.push({
        x: 540,
        y: 100
    });
    this.coinPlaces.push({
        x: 710,
        y: 100
    });
    //-------------------------------
}

function level4o3(){
	this.limitTime = 30;//seconds
	this.eqDifficulty = "hard";
	this.background = background4;
	this.nextLevel = 'menu';
	this.initialX = 0;
	this.initialY = 500;
	
    this.boxes = [];//platforms and borders
    this.boxes.push({
        x: 20,
        y: 180,
        width: 450,
        height: 20
    });
    this.boxes.push({
        x: 640,
        y: 300,
        width: 30,
        height: 30
    });
    this.boxes.push({
        x: 20,
        y: 370,
        width: 450,
        height: 20
    });
    this.boxes.push({
        x: 700,
        y: 450,
        width: 30,
        height: 30
    });
    this.boxes.push({
        x: 0,
        y: 0,
        width: 0,
        height: height
    });
    this.boxes.push({
        x: 0,
        y: height -20,
        width: width,
        height: 20
    });
    this.boxes.push({
        x: width,
        y: 0,
        width: 0,
        height: height
    });
    
    this.enemies = [];
	
this.enemies.push({
        x: this.boxes[0].x,
        y: this.boxes[0].y,
		velocity: 0.04,
		range: (this.boxes[0].width)/2,
		proportion: 0,
		inclination: 0
        //c: this.boxes[0].x + (this.boxes[0].width)/2,
        //range: (this.boxes[0].width)/2 
    });
    this.enemies.push({
        x: this.boxes[2].x+180,
        y: this.boxes[2].y,
		velocity: 0.04,
		range: 65,
		proportion: 0,
		inclination: 0.5
    });
	this.enemies.push({
        x: this.boxes[2].x+300,
        y: this.boxes[2].y,
		velocity: 0.05,
		range: 65,
		proportion: 0,
		inclination: 0.5 
    });
	this.enemies.push({
        x: this.boxes[2].x+420,
        y: this.boxes[2].y,
		velocity: 0.06,
		range: 65,
		proportion: 0,
		inclination: 0.5
	});
	this.enemies.push({
        x: this.boxes[1].x-30,
        y: this.boxes[1].y-100,
		velocity: 0.04,
		range: (this.boxes[1].width)+170,
		proportion: 0.4,
		inclination: -0.40
	});
    this.flyingEnemies = [];
    this.flyingEnemies.push({
        x: 600,
        y: 330,
        c: this.boxes[1].x + (this.boxes[1].width)/2,
        range: (this.boxes[1].width)/2 
    });
    //-------------------------------

    this.doors = [];

	this.doors.push({
        x: 100,
        y: 250,
		nextLevel: this.nextLevel
    });
	
    this.coinPlaces = [];
    
    this.coinPlaces.push({
        x: 200,
        y: 50
    });
    this.coinPlaces.push({
        x: 650,
        y: 100
    });
    this.coinPlaces.push({
        x: 750,
        y: 250
    });
    this.coinPlaces.push({
        x: 250,
        y: 300
    });
    //-------------------------------
}    
    
  