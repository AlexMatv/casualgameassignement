function Door(x,y,nextLevel){
   
    this.x = x;
    this.y = y;
    this.height = 70;
    this.width = 70;
	this.text = "test";
    this.answer = null; 
	this.nextLevel = nextLevel;
	this.locked = false;
    console.log("door created: x = "+this.x+" y = "+this.y);
}

Door.prototype.draw = function(context){
    context.save();
    //context.translate(this.x, this.y);
    //context.rotate(this.rotation);
    //context.scale(this.scaleX, this.scaleY);
	context.beginPath();
    if(this.locked == false){
		var img = new Image();
		img.src = "Images/portal.png";
		context.drawImage(img, this.x, this.y,this.width,this.height);
		context.fillStyle = "white";
		ctx.font = "20px Arial";
		ctx.textAlign = "center";
		ctx.textBaseline = "bottom";
		context.fillText(this.text,this.x + this.width/2 ,this.y);
	}
    context.closePath();
    context.restore(); 
};

