function colDetectionPlatform(player, box) {
// get the vectors to check against
var vX = (player.x + (player.width / 2)) - (box.x + (box.width / 2)),
    vY = (player.y + (player.height / 2)) - (box.y + (box.height / 2)),
    // add the half widths and half heights of the objects
    hWidths = (player.width / 2) + (box.width / 2),
    hHeights = (player.height / 2) + (box.height / 2),
    colDir = null;

// if the x and y vector are less than the half width or half height, they we must be inside the object, causing a collision
if (Math.abs(vX) < hWidths && Math.abs(vY) < hHeights) {
    // figures out on which side we are colliding (top, bottom, left, or right)
    var oX = hWidths - Math.abs(vX),
        oY = hHeights - Math.abs(vY);
    if (oX >= oY) {
        if (vY > 0) {
            colDir = "t";
            player.velY = Math.abs(player.velY);
            player.y += oY;
        } else {
            colDir = "b";
            player.grounded = true;
            player.jumping = false;
            player.y -= oY;
        }
    } else {
        if (vX > 0) {
            colDir = "l";
            player.velX = 0;
            player.x += oX;
        } else {
            colDir = "r";
            player.velX = 0;
            player.x -= oX;
        }
    }
}
return colDir;
}
function colDetectionCoin(player, coin) {
// get the vectors to check against
var vX = (player.x + (player.width / 2)) - (coin.x + (coin.width / 2)),
    vY = (player.y + (player.height / 2)) - (coin.y + (coin.height / 2)),
    // add the half widths and half heights of the objects
    hWidths = (player.width / 2) + (coin.width / 2),
    hHeights = (player.height / 2) + (coin.height / 2);
    //colDir = null;

// if the x and y vector are less than the half width or half height, they we must be inside the object, causing a collision
if (Math.abs(vX) < hWidths && Math.abs(vY) < hHeights) {
    // figures out on which side we are colliding (top, bottom, left, or right)
  if(!player.holdingKey&&coin.visible){
	  mySoundcoin.play();
      player.holdingKey = coin;
      //player.holdingKey = true;
      player.value = player.holdingKey.value;
      coin.visible = false;
      console.log("coin caught");
      console.log("value = "+player.holdingKey.value);
  }
}
//return colDir;
}

function colDetectionDoor(player, door) {
	// get the vectors to check against
	var vX = (player.x + (player.width / 2)) - (door.x + (door.width / 2)),
		vY = (player.y + (player.height / 2)) - (door.y + (door.height / 2)),
		// add the half widths and half heights of the objects
		hWidths = (player.width / 2) + (door.width / 2),
		hHeights = (player.height / 2) + (door.height / 2);
		//colDir = null;

	// if the x and y vector are less than the half width or half height, they we must be inside the object, causing a collision
	if (Math.abs(vX) < hWidths*.3 && Math.abs(vY) < hHeights*.8) {
		
		return true;
		//------------------------------
		// figures out on which side we are colliding (top, bottom, left, or right)
	  if(player.holdingKey){
		  if(player.value == door.answer){
			  mySounddoor.play();
			  return true;
		  }
		  else{
			  console.log("Wrong coin!");
		  }
	  }
	  //------------------------------
	}
	return false;
}

function colDetectionEnemy(player, enemy) {
// get the vectors to check against
var vX = (player.x + (player.width / 2)) - (enemy.x + (enemy.width / 2)),
    vY = (player.y + (player.height / 2)) - (enemy.y + (enemy.height / 2)),
    // add the half widths and half heights of the objects
    hWidths = (player.width / 2) + (enemy.width / 2),
    hHeights = (player.height / 2) + (enemy.height / 2),
    colDir = null;

// if the x and y vector are less than the half width or half height, they we must be inside the object, causing a collision
if (Math.abs(vX) < hWidths*.8 && Math.abs(vY) < hHeights*.8) {
    // figures out on which side we are colliding (top, bottom, left, or right)
	return true;
}
return false;
}