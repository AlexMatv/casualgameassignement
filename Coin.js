function Coin(x,y,value){
    this.x = x;
    this.y = y;
    this.width = 40;
    this.height = 40;
    this.value = value;
    this.visible = true;
    console.log("coin created: x = "+this.x+" y = "+this.y+" value = "+this.value);
}

Coin.prototype.draw = function (context){
    context.save();
    //context.translate(this.x, this.y);
    //context.rotate(this.rotation);
    //context.scale(this.scaleX, this.scaleY);
    context.lineWidth = 3;
    context.strokeStyle = "black";
    context.fillStyle = "blue";
    context.beginPath();
	context.fillStyle = "black";
	//context.fillRect(this.x, this.y, this.width, this.height);
var img = new Image();
    img.src = "Images/crystal.png";
	ctx.textAlign = "center";
	ctx.textBaseline = "middle";
	context.drawImage(img, this.x, this.y);
	ctx.font = "20px Arial";
	context.fillText(this.value,this.x+this.width/2 ,this.y+this.height/2);
    context.closePath();
    context.restore();  
};

