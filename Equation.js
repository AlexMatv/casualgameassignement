function Equation(){
this.formula = null;
this.answer = null;
this.alternatives = [];
this.difficulty
}

Equation.prototype.Generate = function(difficulty){
	var plus = 0,
		minus = 1,
		times = 2,
		divided = 3,
		plusTimes = 4,
		operation = null;
		this.difficulty = difficulty
	switch (this.difficulty){
		case "easy":
			operation = RandomBetween(plus,minus);
			break;
			
		case "medium":
			operation = RandomBetween(plus,divided);
			break;
			
		case "hard":
			operation = RandomBetween(plus,plusTimes);
			break;
	}
	switch (operation){
		case (plus):
			this.Plus();
			break;
		
		case (minus):
			this.Minus();
			break;
			
		case (times):
			this.Times();
			break;
			
		case (divided):
			this.Divided();
			break;
			
		case (plusTimes):
			this.PlusTimes();
			break;
	}
	console.log('formula: '+this.formula + 'dificulty: '+this.difficulty);
}

Equation.prototype.Plus = function(){
	switch (this.difficulty){
		case 'easy':
			var n1 = RandomBetween(5,10);
			var n2 = RandomBetween(1,10);
		break;
		case 'medium':
			var n1 = RandomBetween(10,30);
			var n2 = RandomBetween(10,20);		
		break;
		case 'hard':
			var n1 = RandomBetween(10,99);
			var n2 = RandomBetween(10,99);
		break;
	}
	
	var Sum = n1 + n2;
	this.formula = n1 + "+" + n2;
	this.answer = Sum;
	this.alternatives[0] = Sum;
	this.alternatives[1] = Sum + RandomBetween(1,5);
	this.alternatives[2] = Sum - RandomBetween(1,Math.min(n1,n2));
}

Equation.prototype.Minus = function(){
	switch (this.difficulty){
		case 'easy':
			var n1 = RandomBetween(5,20);
			var n2 = RandomBetween(1,n1-1);
		break;
		case 'medium':
			var n1 = RandomBetween(10,30);
			var n2 = RandomBetween(1,n1-1);		
		break;
		case 'hard':
			var n1 = RandomBetween(1,9) + 10*RandomBetween(1,9);
			var n2 = RandomBetween(1,n1-1);	
		break;
	}
	
	var Sub = n1 - n2;
	this.formula = n1 + "-" + n2;
	this.answer = Sub;
	this.alternatives[0] = Sub;
	this.alternatives[1] = Sub + RandomBetween(1,5);
	this.alternatives[2] = Sub - RandomBetween(1,Sub);
}

Equation.prototype.Times = function(){
	switch (this.difficulty){
		case 'easy':
			//var n1 = RandomBetween(2,10);
			//var n2 = RandomBetween(2,10);
		break;
		case 'medium':
			var n1 = RandomBetween(2,10);
			var n2 = RandomBetween(2,30);		
		break;
		case 'hard':
			var n1 = RandomBetween(10,99);
			var n2 = RandomBetween(2,9);	
		break;
	}
	var n1 = RandomBetween(1,10);
	var n2 = RandomBetween(1,10);
	var Product = n1 * n2;
	this.formula = n1 + "*" + n2;
	this.answer = Product;
	this.alternatives[0] = Product;
	this.alternatives[1] = Product + 10;
	this.alternatives[2] = Product - RandomBetween(1,Product*.1);
	while(this.alternatives[1]===this.alternatives[2]){
		this.alternatives[2] = Product - RandomBetween(1,Product*.1);
	}
}

Equation.prototype.Divided = function(){
		switch (this.difficulty){
		case 'easy':
			//var n1 = RandomBetween(2,10);
			//var n2 = RandomBetween(2,10);
		break;
		case 'medium':
			var quocient = RandomBetween(2,10);
			var divisor = RandomBetween(6,10);		
		break;
		case 'hard':
			var quocient = RandomBetween(10,20) ;
			var divisor = RandomBetween(2,9);	
		break;
		}
	//var quocient = RandomBetween(1,10);
	//var divisor = RandomBetween(1,10);
	var dividend = divisor * quocient;
	this.formula = dividend + "/" + divisor;
	this.answer = quocient;
	this.alternatives[0] = quocient;
	this.alternatives[1] = quocient + RandomBetween(1,5);
	this.alternatives[2] = quocient - RandomBetween(1,quocient-1);
}

Equation.prototype.PlusTimes = function(){
	var n1 = RandomBetween(2,10);
	var n2 = RandomBetween(2,10);
	var n3 = RandomBetween(2,10);
	var Result = n1 + n2 * n3;
	this.formula = n1 + "+" + n2 + "*" + n3;
	this.answer = Result;
	this.alternatives[0] = Result;
	this.alternatives[1] = (n1 + n2) * n3;
	this.alternatives[2] = Result + RandomBetween(-Result,Result);
	while(this.alternatives[1]===this.alternatives[2]){
		this.alternatives[2] = Result + RandomBetween(-Result,Result);
	}
	
}


function RandomBetween(min, max){
	return Math.floor(Math.random()*(max-min+1)+min);
}